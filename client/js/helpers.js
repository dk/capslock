Template.monitor.helpers({
  messages: function () {
    return Messages.find({});
  }
});

Template.monitor.events({
  'click .done': function(event) {
    var id = '#m' + event.currentTarget.id;
    $(id).css('background-color', '#ec0');
    new Confirmation({
      title: "Clear this request?",
      message: "please confirm",
      cancelText: "Cancel",
      okText: "Ok",
      success: true
    }, function (ok) {
      if (ok) {
        Messages.remove({ _id: event.currentTarget.id });
      } else {
        $(id).css('background-color', '#000');
      }
    });
  }
});

Template.security.helpers({
  PIN: function() {
    return Session.get('PIN');
  }
});

Template.security.events({
  'submit #pinentry': function(event) {
     Session.set('PIN', event.target.PIN.value);
     return false;
  }
});

